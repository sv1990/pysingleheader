import os
import os.path
import re
import sys


def find_if(iterable, predicate):
    for ind, val in enumerate(iterable):
        if predicate(val):
            return ind
    return None


def header_sort_key(header):
    path, file = os.path.split(header)
    file, ext = os.path.splitext(file)
    return path, ext, file


def add_internal_dependency(internal_dependencies, new_header):
    if new_header.location not in internal_dependencies:
        internal_dependencies[new_header.location] = new_header
        return
    old_header = internal_dependencies[new_header.location]
    if old_header.conditions == new_header.conditions:
        return
    if len(old_header.conditions) == 0 or len(new_header.conditions) == 0:
        old_header.conditions = []
        return
    raise Exception(
        f'Incompatible conditions for inclusion of {old_header.name}')


def filter_header_guards(lines):
    include_guard_index = find_if(lines, lambda line: '#ifndef' in line)
    if include_guard_index is not None:
        include_guard_end = find_if(reversed(lines),
                                    lambda line: '#endif' in line)
        lines.pop(-include_guard_end - 1)
        lines.pop(include_guard_index + 1)
        lines.pop(include_guard_index)
    return lines


def make_header_guard(name):
    return 'SINGLE_HEADER_{}'.format(
        ''.join(c if c.isalnum() else '_' for c in name.upper()))


class Header:
    def __init__(self, location, conditions=None):
        self.name = os.path.basename(location)
        self.location = os.path.realpath(location)
        self.root = os.path.dirname(self.location)
        self.internal_dependencies = dict()
        self.external_dependencies = set()
        self.content = []
        self.conditions = conditions if conditions is not None else []
        self.initialize_dependencies()

    def __repr__(self):
        return self.location

    def initialize_dependencies(self):
        internal_rgx = re.compile(r'#\s*include\s*\"(.*)\"')
        external_rgx = re.compile(r'#include\s*<(.*)>')
        if_rgx = re.compile(r'#\s*((ifn?def)|(if)).*')
        endif_rgx = re.compile(r'#\s*endif')
        with open(self.location) as f:
            file_content = filter_header_guards(f.read().split('\n'))
        for line in file_content:
            # TODO: Replace by statement expressions to emulate if with
            #  initializer as soon as python 3.8 is out
            match = re.search(internal_rgx, line)
            if match:
                filename = os.path.realpath(
                    f'{self.root}/{match.group(1)}')
                new_header = Header(filename, conditions=self.conditions.copy())
                add_internal_dependency(self.internal_dependencies, new_header)
                continue
            match = re.search(external_rgx, line)
            if match:
                self.external_dependencies.add(match.group(1))
                continue
            match = re.match(if_rgx, line)
            if match:
                self.conditions.append(line)
                self.content.append(line)
                continue
            match = re.match(endif_rgx, line)
            if match:
                self.conditions.pop()
                self.content.append(line)
                continue
            self.content.append(line)

    def collect_external_dependencies(self):
        dependencies = set(self.external_dependencies)
        for header in self.internal_dependencies.values():
            dependencies = dependencies.union(
                header.collect_external_dependencies())
        return dependencies

    def dump_header(self, file=sys.stdout, dumped=None, root_header=True,
                    license_file=None):
        if dumped is None:
            dumped = set()
        if root_header:
            if license_file is not None:
                file.writelines(f'// {line}' for line in license_file)
                file.write('\n')
            header_guard = make_header_guard(self.name)
            file.write(f'#ifndef {header_guard}\n#define {header_guard}\n')
            external_dependencies = sorted(self.collect_external_dependencies(), key=header_sort_key)
            file.writelines(f'#include <{dependency}>\n' for dependency in
                            external_dependencies)
        for dependency in self.internal_dependencies.values():
            dependency.dump_header(file=file, dumped=dumped, root_header=False)
        if self.location not in dumped:
            file.writelines(f'{condition}\n' for condition in self.conditions)
            file.write('\n'.join(self.content))
            file.writelines('#endif\n' for _ in self.conditions)
        dumped.add(self.location)
        if root_header:
            file.write(f'#endif // {header_guard}\n')


if __name__ == '__main__':
    import argparse
    import pathlib

    def map_or(func, x, or_else=None):
        if x is not None:
            return func(x)
        else:
            return or_else

    def close(x):
        return x.close()

    def openw(x):
        return open(x, 'w')

    parser = argparse.ArgumentParser(
        description='Generate a single header library')
    parser.add_argument('input',
                        help='Input header file to generate the single header')
    parser.add_argument('-o', '--output',
                        help='Destination for the single header. Prints to console if none is provided')
    parser.add_argument('-l', '--license',
                        help='Destination of a license file to be attached on top of the single header')
    args = parser.parse_args()
    initial_header = Header(args.input)

    if args.output:
        pathlib.Path(args.output).parent.mkdir(parents=True, exist_ok=True)

    output_file = map_or(openw, args.output, or_else=sys.stdout)
    license_file = map_or(open, args.license, or_else=None)
    initial_header.dump_header(file=output_file, license_file=license_file)
    map_or(close, license_file)
    map_or(close, output_file)
