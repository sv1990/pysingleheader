#ifndef SINGLE_HEADER_TEST_A_HH_1556567261825743391_
#define SINGLE_HEADER_TEST_A_HH_1556567261825743391_

#include "b.hh"

#include <string>
#include <algorithm>
#include <range/v3/all.hpp>

struct a {
  b b;
};

#endif // SINGLE_HEADER_TEST_A_HH_1556567261825743391_
