#ifndef SINGLE_HEADER_TEST_MAIN_HH_1556567339319039319_
#define SINGLE_HEADER_TEST_MAIN_HH_1556567339319039319_

#include "a.hh" // a
#include "b.hh"

#if A
#  ifdef B
#    include "c.hh"
#  endif
#endif

#include <string>

#if R
#  include <regex>
#endif

struct x {
  a a;
  b b;
};

#endif // SINGLE_HEADER_TEST_MAIN_HH_1556567339319039319_
